package com.example.vkmessenger;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;

import com.example.vkmessenger.utils.Constants;
import com.example.vkmessenger.utils.ShowMessage;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKRequest.VKRequestListener;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.httpClient.VKHttpClient;
import com.vk.sdk.api.photo.VKImageParameters;
import com.vk.sdk.api.photo.VKUploadImage;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Bitmap;

public class SendMessage extends AsyncTask<Message, Void, Boolean>
{
	public static final int MAX_BUFFER_SIZE = 2*1024*1024;
	public static final int SC_OK=200;
	private InputStream uploadFileIS;
	
	private FragmentManager fragmentManager;
	private Context context;
	
	public SendMessage(Context context, FragmentManager fragmentManager)
	{
		this.fragmentManager=fragmentManager;
		this.context=context;
	}
	@Override
	protected Boolean doInBackground(Message... params) 
	{
		if(params.length==0)
			return false;
		Message message=params[0];
		URL url;
		InputStream inputStream=null;
		InputStreamReader isReader=null;
		try 
		{
			// getting store
			url = new URL(Constants.VK_API_ADDRESS+"photos.getMessagesUploadServer?access_token="+message.accessToken);
			JsonParser jsonParser=new JsonParser();
			inputStream=url.openStream();
			isReader=new InputStreamReader(inputStream);
			JsonElement response=jsonParser.parse(isReader).getAsJsonObject().get("response");
			String uploadUrl=response.getAsJsonObject().get("upload_url").getAsString();
			isReader.close();
			
			// upload img file
			List<PhotoDesc> uploadPhotos=null;
			if(message.image!=null)
			{
				VKUploadImage vkUploadImage=new VKUploadImage(message.image, VKImageParameters.jpgImage(0.9f));

				HttpPost httpPost=VKHttpClient.fileUploadRequest(uploadUrl, vkUploadImage.getTmpFile());//new File(message.pathToImage));
				HttpResponse httpResponse = VKHttpClient.getClient().execute(httpPost);
		        HttpEntity resEntity = httpResponse.getEntity();
				String photoSaveRes=EntityUtils.toString(resEntity).trim();
				
				JsonElement photoJSON=jsonParser.parse(photoSaveRes).getAsJsonObject();
				
				String server=photoJSON.getAsJsonObject().get("server").getAsString();
			    String photo=photoJSON.getAsJsonObject().get("photo").getAsString();
			    String hash=photoJSON.getAsJsonObject().get("hash").getAsString();

				url=new URL(Constants.VK_API_ADDRESS+"photos.saveMessagesPhoto?"+
						String.format("server=%s&photo=%s&hash=%s&access_token=%s", server, photo, hash, message.accessToken));
				Gson gson=new Gson();
				inputStream=url.openStream();
				isReader=new InputStreamReader(inputStream);
				response=jsonParser.parse(isReader).getAsJsonObject().get("response");
				Type collectionType=new TypeToken<List<PhotoDesc> >(){}.getType();
				uploadPhotos=gson.fromJson(response, collectionType);
				isReader.close();
				if(uploadPhotos.isEmpty())
					return false;
			}
			// send message
			String sendMsgUrl=Constants.VK_API_ADDRESS+"messages.send?"+
					String.format("user_id=%s&message=%s&access_token=%s", message.receiverUserId, message.text, message.accessToken);
			if(message.image!=null && !uploadPhotos.isEmpty())
				sendMsgUrl+=String.format("&attachment=%s", uploadPhotos.get(0).id);
			url=new URL(sendMsgUrl);
			inputStream=url.openStream();
			isReader=new InputStreamReader(inputStream);
			inputStream.close();
			return true;
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (JsonIOException e) {
			e.printStackTrace();
		} catch (JsonSyntaxException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally
		{
			try 
			{
				if(isReader!=null) isReader.close();
			} catch (IOException e) 
			{
				e.printStackTrace();
			}	
		}
		return false;
	}

	@Override
    protected void onPostExecute(Boolean result) 
    {
        super.onPostExecute(result);
        if(!result)
        	ShowMessage.showMessage(fragmentManager, context.getString(R.string.error_title), context.getString(R.string.send_message_error), context.getString(R.string.success_button));
    }	
}
