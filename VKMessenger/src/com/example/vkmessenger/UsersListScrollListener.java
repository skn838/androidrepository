package com.example.vkmessenger;

import com.example.vkmessenger.utils.ImagesManager;
import com.example.vkmessenger.utils.OnDoneAsyncTask;

import android.app.Activity;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;

public class UsersListScrollListener implements OnScrollListener 
{

	    public static final int VISIBLE_THRESHOLD = 15;
	    private int currentPage = 0;
	    private int previousTotal = 0;
	    private boolean loading = true;
	    private FriendsLoader friendsLoader;
	    private String userId;
	    private String apiVer;
	    private Activity activity;
	    private OnDoneAsyncTask doneAsyncTask;
	    private ImagesManager imagesManager;
	    
	    private LoadFriends loadFriends;
	    
	    public interface LoadFriends
	    {
	    	void loadFriends(int currentPage);
	    }
	    
	    public UsersListScrollListener(LoadFriends loadFriends) 
	    {
	    	this.loadFriends=loadFriends;
	    }

	    @Override
	    public void onScroll(AbsListView view, int firstVisibleItem,
	            int visibleItemCount, int totalItemCount) {
	        if (loading) {
	            if (totalItemCount > previousTotal) {
	                loading = false;
	                previousTotal = totalItemCount;
	                currentPage++;
	            }
	        }
	        if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + VISIBLE_THRESHOLD)) 
	        {
	        	loadFriends.loadFriends(currentPage);
	            loading = true;
	        }
	    }
		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) 
		{
		}
}
