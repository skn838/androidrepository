package com.example.vkmessenger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.example.vkmessenger.utils.Constants;
import com.example.vkmessenger.utils.SelectImageDialog;
import com.example.vkmessenger.utils.ShowMessage;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKSdk;
import com.vk.sdk.VKUIHelper;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.Media;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;

public class SendMessageActivity extends Activity
{
	private static final int FROM_GALLERY_CODE=0,
							 FROM_CAMERA_CODE=1;
	
	public static final String SEND_IMG="send_img",
							   IMG_IS_SELECTED="img_is_selected",
							   TAG="MY_LOG";
	
	private boolean imgIsSelected=false;
	private String receiverId;
	private String sendImgFilePath="";
    
    
	SelectImageDialog selectImageDialog;
	
	OnClickListener selectImageListener = new OnClickListener() 
	{
		@Override
		public void onClick(View rButton) 
		{
			RadioButton radioButton = (RadioButton)rButton;
			switch (radioButton.getId()) 
			{
			case R.id.fromGalleryRB: 
				Intent intent = new Intent(Intent.ACTION_PICK);
		        intent.setType("image/*");
		        startActivityForResult(Intent.createChooser(intent,"Select file to upload "), FROM_GALLERY_CODE);
			    break;
			case R.id.fromCameraRB: 
				Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			    startActivityForResult(cameraIntent, FROM_CAMERA_CODE);
			    break;
			default:
				break;
			}
			selectImageDialog.dismiss();
		}
	};
	
	@Override
	protected void onResume() 
	{
		super.onResume();
		VKUIHelper.onResume(this);
	}

	@Override
	protected void onDestroy() 
	{
		super.onDestroy();
		VKUIHelper.onDestroy(this);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		super.onActivityResult(requestCode, resultCode, data);
		VKUIHelper.onActivityResult(this, requestCode, resultCode, data);
		Bitmap bitmap=null;
		
		if(resultCode==RESULT_OK)
		{
			switch(requestCode)
			{
			 
			case FROM_CAMERA_CODE:
		        bitmap = (Bitmap) data.getExtras().get("data");
		        break;
		    case FROM_GALLERY_CODE:
		    	Uri selectedImageUri = data.getData();
		    	try 
		    	{
					bitmap = Media.getBitmap(getContentResolver(), selectedImageUri);
				} catch (IOException e) 
		    	{
					e.printStackTrace();
					ShowMessage.showMessage(getFragmentManager(), getString(R.string.error_title),
							getString(R.string.from_gallery_error), getString(R.string.success_button));
				}
		        break;
			}
			if(bitmap!=null)
			{	
				ImageView image=(ImageView)findViewById(R.id.sendImage);
	        	image.setImageBitmap(bitmap);
	        	image.setMaxWidth((int)getResources().getDimension(R.dimen.max_send_img_size));
	        	image.setMaxHeight((int)getResources().getDimension(R.dimen.max_send_img_size));
	        	imgIsSelected=true;
			}
		}
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_send_message);
		
		Intent intent=getIntent();
		String userName=intent.getStringExtra(Constants.USER_NAME_P);
		receiverId=intent.getStringExtra(Constants.RECEIVER_ID);
		
		if(userName!=null)
			this.getActionBar().setTitle(userName);
		selectImageDialog=new SelectImageDialog(selectImageListener, getString(R.string.select_image_title), getString(R.string.cancel));
		
		ImageView sendImage=(ImageView)findViewById(R.id.sendImage);		
		sendImage.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) 
			{
				selectImageDialog.show(getFragmentManager(), "SELECT_IMG");
			}		
		});

		Button selfSendButton=(Button)findViewById(R.id.selfSend);
		selfSendButton.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v) 
			{
				VKAccessToken accessToken=VKSdk.getAccessToken();
				sendMessage(accessToken.userId);
			}
			
		});
	}
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
        getMenuInflater().inflate(R.menu.send_msg_menu, menu);
        
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) 
    {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        if(id==R.id.send_msg_button)
        {
        	sendMessage(receiverId);
        }
        return super.onOptionsItemSelected(item);
    }
    
    @Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) 
	{
		super.onRestoreInstanceState(savedInstanceState);
		ImageView sendImg=(ImageView)findViewById(R.id.sendImage);
		sendImg.setImageBitmap((Bitmap)savedInstanceState.getParcelable(SEND_IMG));
		imgIsSelected=savedInstanceState.getBoolean(IMG_IS_SELECTED);
		receiverId=savedInstanceState.getString(Constants.RECEIVER_ID);
	}
    
    @Override
	protected void onSaveInstanceState(Bundle outState) 
	{
		super.onSaveInstanceState(outState);
		ImageView sendImg=(ImageView)findViewById(R.id.sendImage);
		outState.putParcelable(SEND_IMG, ((BitmapDrawable)sendImg.getDrawable()).getBitmap());
		outState.putBoolean(IMG_IS_SELECTED, imgIsSelected);
		outState.putString(Constants.RECEIVER_ID, receiverId);
	}
    
    private void sendMessage(String receiverId)
    {
    	EditText messageText=(EditText)findViewById(R.id.messageText);
    	Bitmap bitmap=null;
    	if(imgIsSelected)
    	{
    		ImageView image=((ImageView)findViewById(R.id.sendImage));
    		bitmap=((BitmapDrawable)image.getDrawable()).getBitmap();
    	}
    	SendMessage sendMessage=new SendMessage(getApplicationContext(), getFragmentManager());
    	VKAccessToken accessToken=VKSdk.getAccessToken();
    	sendMessage.execute(new Message(accessToken.userId, messageText.getText().toString(), receiverId,
    			sendImgFilePath, accessToken.accessToken, bitmap));
    }
}
