package com.example.vkmessenger;

import java.util.ArrayList;
import java.util.List;

import com.example.vkmessenger.utils.ImagesManager;
import com.example.vkmessenger.utils.OnDoneAsyncTask;
import com.example.vkmessenger.utils.ShowMessage;
import com.google.gson.Gson;

import android.app.FragmentManager;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class UsersList extends ListFragment implements OnDoneAsyncTask<List<User> >
{
	protected FriendsLoader friendsLoader;
	protected ImagesManager imagesManager;
	private UsersListArrayAdapter adapter;
	private UsersListScrollListener scrollListener;
	
	private class LoadFriendsListener implements UsersListScrollListener.LoadFriends
	{
		String userId;
		
		public LoadFriendsListener(String userId)
		{
			this.userId=userId;
		}
		
		public void loadFriends(int currentPage)
		{
			friendsLoader=new FriendsLoader(UsersList.this.getActivity(), UsersList.this, imagesManager);
    		friendsLoader.execute("user_id="+userId, "count="+UsersListScrollListener.VISIBLE_THRESHOLD, 
    				"offset="+currentPage*UsersListScrollListener.VISIBLE_THRESHOLD, "fields=photo_50,online", 
    				"version="+getString(R.string.vk_api_ver));
		}
	}
	
	private LoadFriendsListener loadFriends;
	
	public UsersList()
	{
	}
	
	public void setUsersLoader(String userId, OnItemClickListener itemClickListener)
	{
		adapter=new UsersListArrayAdapter(this.getActivity().getApplicationContext());
		setListAdapter(adapter);
		imagesManager=new ImagesManager();
		
		loadFriends=new LoadFriendsListener(userId);
		
		loadFriends.loadFriends(0);
		this.getListView().setChoiceMode(ListView.CHOICE_MODE_NONE);
		scrollListener=new UsersListScrollListener(loadFriends);
		this.getListView().setOnScrollListener(scrollListener);
		this.getListView().setOnItemClickListener(itemClickListener);
	}

	/*public void update(List<User> users)
	{
		
	}*/
	
	@Override
	public void doneAsyncTask(List<User> users)
	{
		
		for(User user : users)
			adapter.add(user);
		//adapter.addAll(users);
		adapter.notifyDataSetChanged();
	}
	
	public void saveUsers(Bundle saveBundle)
	{
		//Bundle users=new Bundle();
		//Adapter adapter=this.getListView().getAdapter();
		ArrayList<User> users=new ArrayList<User>();
		for(int i=0; i<adapter.getCount(); ++i)
			users.add((User)adapter.getItem(i));
		//Bundle res=new Bundle();
		saveBundle.putParcelableArrayList("users", users);
		if(getListView().getChildCount()>0)
			saveBundle.putInt("firstVisiblePosition", getListView().getPositionForView(getListView().getChildAt(0)));
		else
			saveBundle.putInt("firstVisiblePosition", -1);
		//return res;
	}
	
	public void readSavedUsers(Bundle saveBundle, FragmentManager fragmentManager)
	{
		ArrayList<User> users=saveBundle.getParcelableArrayList("users");
		adapter.clear();
		adapter.addAll(users);
		adapter.notifyDataSetChanged();
		Gson gson=new Gson();
		/*if(!users.isEmpty())
			ShowMessage.showMessage(fragmentManager, "Object to JSON", gson.toJson(users.get(0)), "ok");*/
		int position=saveBundle.getInt("firstVisiblePosition");
		getListView().setSelection(position);
	}
}
