package com.example.vkmessenger;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable
{
	public String   user_id,
					first_name,
					last_name,
					photo_50;
	public int online;
	
	public Bitmap photo;
	
	public String getFullName()
	{
		return first_name+" "+last_name;
	}
	@Override
	public String toString()
	{
		return String.format("id=%s; name=%s %s; photo_url=%s, isOnline=%s", user_id, first_name, last_name, photo_50, online);
	}
	@Override
	public int describeContents() 
	{
		return 0;
	}
	@Override
	public void writeToParcel(Parcel dest, int flags) 
	{
		dest.writeString(user_id);
		dest.writeString(first_name);
		dest.writeString(last_name);
		dest.writeString(photo_50);
		dest.writeInt(online);
		dest.writeParcelable(photo, flags);
	}
	
	public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>()
	{
		public User createFromParcel(Parcel in) {
			return new User(in);
		}

		public User[] newArray(int size) {
			return new User[size];
		}
	};

	private User(Parcel parcel) 
	{
		user_id=parcel.readString();
		first_name=parcel.readString();
		last_name=parcel.readString();
		photo_50=parcel.readString();
		online=parcel.readInt();
		photo=(Bitmap)parcel.readParcelable(getClass().getClassLoader());
	}
}
