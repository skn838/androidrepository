package com.example.vkmessenger.utils;

import android.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Build;
import android.os.Bundle;

public class ShowMessage 
{
	public static void showMessage(FragmentManager fragmentManager, String title, String message, String successButton)
	{
		DialogFragment dialog=new CustomDialog(title, message, successButton);
		dialog.show(fragmentManager, "customDialog");
	}
}
