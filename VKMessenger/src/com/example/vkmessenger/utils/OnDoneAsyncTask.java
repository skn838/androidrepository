package com.example.vkmessenger.utils;

public interface OnDoneAsyncTask<ResultType>
{
	void doneAsyncTask(ResultType result);
}
