package com.example.vkmessenger.utils;

import com.example.vkmessenger.R;

import android.annotation.TargetApi;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.view.View.OnClickListener;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class CustomDialog extends DialogFragment implements OnClickListener
{
	String title, message, successButton;

	public CustomDialog(String title, String message, String successButton) 
	{
		super();
		this.title = title;
		this.message = message;
		this.successButton = successButton;
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) 
	{
		getDialog().setTitle(title);
		View cDialog = inflater.inflate(R.layout.custom_dialog, null);
		Button successButton=(Button)cDialog.findViewById(R.id.successDialogButton);
		successButton.setText(this.successButton);
		successButton.setOnClickListener(this);
		((TextView)cDialog.findViewById(R.id.textDialog)).setText(message);
		return cDialog;
	}

	@Override
	public void onClick(View v) 
	{
		dismiss();
	}
}
