package com.example.vkmessenger.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class FeaturesTests 
{
	public static boolean isNetworkConnected(Context context)
    {
        ConnectivityManager connManager=(ConnectivityManager)context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo=connManager.getActiveNetworkInfo();
        if(networkInfo==null)
            return false;
        else
            return true;
    }
}
