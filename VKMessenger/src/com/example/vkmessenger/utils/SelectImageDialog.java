package com.example.vkmessenger.utils;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.vkmessenger.R;

public class SelectImageDialog extends DialogFragment implements OnClickListener
{
	String title, successButton;
	OnClickListener selectImageListener;
	
	
	public SelectImageDialog(OnClickListener selectImageListener, String title, String successButton) 
	{
		super();
		this.title = title;
		this.successButton = successButton;
		this.selectImageListener=selectImageListener;
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) 
	{
		getDialog().setTitle(title);
		View cDialog = inflater.inflate(R.layout.select_image_dialog, null);
		Button successButton=(Button)cDialog.findViewById(R.id.successDialogButton);
		successButton.setText(this.successButton);
		successButton.setOnClickListener(this);
		cDialog.findViewById(R.id.fromGalleryRB).setOnClickListener(selectImageListener);
		cDialog.findViewById(R.id.fromCameraRB).setOnClickListener(selectImageListener);
		return cDialog;
	}

	@Override
	public void onClick(View v) 
	{
		dismiss();
	}
}
