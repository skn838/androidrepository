package com.example.vkmessenger.utils;

public class Constants 
{
	public static final int PROGRESS_DIALOG_ID=1;
	public static final String USER_NAME_P="userName",
							   VK_API_ADDRESS="https://api.vk.com/method/",
							   RECEIVER_ID="receiverId";
}
