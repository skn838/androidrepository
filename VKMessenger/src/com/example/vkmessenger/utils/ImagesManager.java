package com.example.vkmessenger.utils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class ImagesManager 
{
	private Map<String, Bitmap> imagesCache;
	
	public ImagesManager()
	{
		imagesCache=new HashMap<String, Bitmap>();
	}
	
	public Bitmap getImage(String urlStr, boolean forceReload) throws IOException
	{
		URL url=new URL(urlStr);
		if(forceReload || imagesCache.get(urlStr)==null)
		{
			Bitmap loadImg=BitmapFactory.decodeStream(url.openStream());
			imagesCache.put(urlStr, loadImg);
			return loadImg;
		}
		else
			return imagesCache.get(urlStr);
	}
}
