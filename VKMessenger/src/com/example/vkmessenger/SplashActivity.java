package com.example.vkmessenger;

import com.vk.sdk.VKUIHelper;

import android.support.v7.app.ActionBarActivity;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;

public class SplashActivity extends Activity 
{
	private static final int WAITING_TIME=3;
	
	@Override
	protected void onResume() 
	{
		super.onResume();
		VKUIHelper.onResume(this);
	}

	@Override
	protected void onDestroy() 
	{
		super.onDestroy();
		VKUIHelper.onDestroy(this);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		super.onActivityResult(requestCode, resultCode, data);
		VKUIHelper.onActivityResult(this, requestCode, resultCode, data);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);

		new Handler().postDelayed(new Runnable()
		{
			public void run()
			{
				Intent mainActivityIntent=new Intent(SplashActivity.this, MainActivity.class);
				SplashActivity.this.startActivity(mainActivityIntent);
				SplashActivity.this.finish();
			}
		}, 1000*WAITING_TIME);
	}
}
