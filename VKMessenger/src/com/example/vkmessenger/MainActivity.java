package com.example.vkmessenger;

import com.example.vkmessenger.utils.FeaturesTests;
import com.example.vkmessenger.utils.ShowMessage;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.VKSdkListener;
import com.vk.sdk.VKUIHelper;
import com.vk.sdk.api.VKError;
import com.vk.sdk.util.VKUtil;

import android.support.v7.app.ActionBarActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity 
{
	//private static final int VK_APPLICATION_ID=4924355;
	
	private NetworkStateReceiver netSateReceiver = new NetworkStateReceiver();
	private boolean isNetSRecRegister=false;
	
	private class NetworkStateReceiver extends BroadcastReceiver
	{
	    @Override
	    public void onReceive( Context context, Intent intent )
	    {
	    	if(FeaturesTests.isNetworkConnected(getApplicationContext()))
	    		startWork();
	    }
	}
	
	private VKSdkListener vkSdkListener=new VKSdkListener()
	{

		@Override
		public void onAccessDenied(VKError error) 
		{
			ShowMessage.showMessage(getFragmentManager(), getString(R.string.error_title), getString(R.string.login_failed), getString(R.string.success_button));
		}

		@Override
		public void onCaptchaError(VKError arg0) 
		{
			
		}

		@Override
		public void onTokenExpired(VKAccessToken error) 
		{
			
		}
		
		public void onReceiveNewToken(VKAccessToken newToken)
		{
			super.onReceiveNewToken(newToken);
			showUsersList(newToken);
		}
	};
	@Override
	protected void onResume() 
	{
		super.onResume();
		VKUIHelper.onResume(this);
	}

	@Override
	protected void onDestroy() 
	{
		super.onDestroy();
		if(isNetSRecRegister)
			getApplicationContext().unregisterReceiver(netSateReceiver);
		VKUIHelper.onDestroy(this);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		super.onActivityResult(requestCode, resultCode, data);
		VKUIHelper.onActivityResult(this, requestCode, resultCode, data);
	}
	
    @Override
    protected void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        if(!FeaturesTests.isNetworkConnected(getApplicationContext()))
        {
        	ShowMessage.showMessage(getFragmentManager(), getString(R.string.error_title), 
        			getString(R.string.not_net_connection), getString(R.string.success_button));
			getApplicationContext().registerReceiver( netSateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION ) );
			isNetSRecRegister=true;
        }
        else
        {
        	startWork();
        }
    }

    private void startWork()
    {
    	VKSdk.initialize(vkSdkListener, getString(R.string.vk_app_id));
        
        VKUIHelper.onCreate(this);
         
        Button vkLoginButton = (Button) findViewById(R.id.vk_login_button);
        vkLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VKSdk.authorize(VKScope.FRIENDS, VKScope.MESSAGES, VKScope.DOCS, VKScope.PHOTOS);
            }
        });
         
        /*
         * if (VKSdk.wakeUpSession()) 
        {
        	vkLoginButton.setText("Already authorized");
        	showUsersList(VKSdk.getAccessToken());
            //startLoading();
        } else 
        {
            vkLoginButton.setVisibility(View.VISIBLE);
        }*/
    }
    private void showUsersList(VKAccessToken accessToken)
    {
    	Intent usersListIntent=new Intent(this, UsersListActivity.class);
    	//usersListIntent.putExtra("accessToken", accessToken.accessToken);
    	startActivity(usersListIntent);
    	finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) 
    {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
