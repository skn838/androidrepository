package com.example.vkmessenger;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.example.vkmessenger.utils.Constants;
import com.example.vkmessenger.utils.ImagesManager;
import com.example.vkmessenger.utils.OnDoneAsyncTask;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

public class FriendsLoader extends AsyncTask<String, Void, List<User> >
{
	private static final String DEBUG_TAG = "DEBUG";
	private Activity activity;
	private OnDoneAsyncTask<List<User>> doneCallback;
	private ImagesManager imagesManager;
	
	public FriendsLoader(Activity activity, OnDoneAsyncTask<List<User> > doneCallback, ImagesManager imagesManager)
	{
		this.activity=activity;
		this.doneCallback=doneCallback;
		this.imagesManager=imagesManager;
	}
	
	@Override
    protected List<User> doInBackground(String... params) 
	{
        publishProgress(new Void[]{});
        List<User> friends=new ArrayList<User>();

        String url = Constants.VK_API_ADDRESS+"friends.get?";
        for(int i=0; i<params.length-1; ++i)
        	url+=params[i]+"&";
        url+=params[params.length-1];
        
       
        InputStream input = null;
        try 
        {
        	URL urlConn = new URL(url);
            input = urlConn.openStream();
            
            Gson gson=new Gson();
	        Type collectionType=new TypeToken<List<User> >(){}.getType();
	        
	        JsonReader jsonReader=new JsonReader(new InputStreamReader(input));
	        jsonReader.beginObject();
	        if(jsonReader.hasNext())
	        {
	        	if(jsonReader.nextName().equals("response"))
			        friends=gson.fromJson(jsonReader, collectionType);
	        }
	        jsonReader.endObject();
        
	        for(User user : friends)
	        {
	        	if(user.photo_50!=null)
	        		user.photo=imagesManager.getImage(user.photo_50, false);
	        }
        }
        catch (MalformedURLException e) {
        	Log.d(DEBUG_TAG,"Something wrong with url.");
        	e.printStackTrace();
        }
        catch (IOException e) {
        	Log.d(DEBUG_TAG,"Something wrong with input stream.");
            e.printStackTrace();
        }

        return friends;
    }
    
	@Override
	protected void onPreExecute()
	{
		ProgressBar progressBar=(ProgressBar)(activity.findViewById(R.id.progressBar1));
		progressBar.setVisibility(View.VISIBLE);
	}
	
    @Override
    protected void onProgressUpdate(Void... values) 
    {
        super.onProgressUpdate(values);
    }
    
    @Override
    protected void onPostExecute(List<User> result) 
    {
        super.onPostExecute(result);
        ProgressBar progressBar=(ProgressBar)(activity.findViewById(R.id.progressBar1));
		progressBar.setVisibility(View.INVISIBLE);
        doneCallback.doneAsyncTask(result);
    }
}
