package com.example.vkmessenger;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class UsersListArrayAdapter extends ArrayAdapter<User>
{
	private final Context context;
    private final LayoutInflater lInflater;
    
    public UsersListArrayAdapter(Context context) 
    {
        super(context, R.layout.users_list_item);
        this.context = context;
        lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) 
    {
        View view = convertView;
        if (view == null)
            view = lInflater.inflate(R.layout.users_list_item, parent, false);

        final ListView lv = (ListView) parent;
        if(position == lv.getCheckedItemPosition())
        {
            view.setBackgroundColor(0x88ccffFF);
        }

        User user=getItem(position);
        ((TextView) view.findViewById(R.id.userName)).setText(user.getFullName());
        if(user.photo!=null)
        	((ImageView) view.findViewById(R.id.userImage)).setImageBitmap(user.photo);
        if(user.online==1)
        	((ImageView)view.findViewById(R.id.isOnline)).setVisibility(View.VISIBLE);
        else
        	((ImageView)view.findViewById(R.id.isOnline)).setVisibility(View.INVISIBLE);
        return view;
    }
}
