package com.example.vkmessenger;

import android.graphics.Bitmap;

public class Message 
{
	public String ownerId,
				  text,
		   		  receiverUserId,
		   		  pathToImage,
		   		  accessToken;
	
	public Bitmap image;
	
	public Message(String ownerId, String text, String receiverUserId, String pathToImage, String accessToken, Bitmap image)
	{
		this.ownerId=ownerId;
		this.text=text;
		this.receiverUserId=receiverUserId;
		this.pathToImage=pathToImage;
		this.accessToken=accessToken;
		this.image=image;
	}
}
